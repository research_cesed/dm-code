# CODEBOOK

La base de datos `42consolidado.csv` está a nivel de municipio, es decir, cada fila representa un municipio de Colombia. El identificador de cada municipio es el código de municipio del DANE que está en la variable `cod_muni` . 

# Diccionario

Cada variable agrega la *cantidad* del delito o hecho respectivo de acuerdo a la unidad de medida del hecho y, en algunos casos, se presenta esta cantidad desagregada por, por ejemplo, zona (rural y urbana), sexo (femenino, masculino, no reporta), tipo delito (secuestro extorsivo, secuestro simple), etc. Para identificar los delitos/hechos que sí tienen desagregaciones se puede revisar la columna **Num. variables desagreg.** de la tabla anterior. 

## Información Agregada

Todas las variables de información **agregada** son de la forma `q_hecho_M_AAAA`, donde:

1. `q`: Es la unidad de agregación. Hay 13 tipos de agregación:
   
   - `n`: Número de eventos/delitos. Toma números naturales.
   
   - `vict`: Número de *víctimas* del evento. Solo presente en el caso de homicidios colectivos
   
   - `ha`: Hectáreas
   
   - `kg`: Kilogramos
   
   - `gal`: Galónes
   
   - `gr`: Gramos
   
   - `lb`: Libras
   
   - `lt`: Litros
   
   - `mt`: Metros lineales
   
   - `m3`: Metros cúbicos
   
   - `pers`: Número de personas
   
   - `u`: Número de unidades, por ejemplo cantidad de unidades de infraestructura para elaboración de droga destruidas.

2. `hecho`: Indica el hecho o delito contabilizado. Esta corresponde a la columna **Nombre variable** de la **Tabla 1**.

3. `M`: Corresponde al mes del suceso. Toma los valores enteros 1 al 12 . No necesariamente para cada hecho existe la variable con los 12 valores, pues puede que el alguna combinación mes-año no se registre información.

4. `AAAA`: Indica el año, por ejemplo "1999" o "2022".

## Información Desagregada

Algunos hechos tienen variables que permiten desagregar la información. Las variables que indican la cantidad desagregada, siguiendo la misma estructura, son de la forma `q_desag_hecho_M_AAAA`, donde `desag` toma los valores de la desagregación respectiva. 

Por ejemplo, la varaible `n_p_vpue_10_2003` representa el número de voladuras *de puentes,* codificado en este caso por `p`, que ocurrieron en Julio del 2003. Al ver la **Tabla 1** se puede observar que este delito agrega puentes y vías, y el archivo respectivo tiene una columna de "clase_de_bien" que permite identificar si se trató de un puente o de una vía. 

### Desagregaciones y opciones

La lista de desagregaciones posibles con sus respectivas categorías y los códigos con los cuáles se codifica en el nombre de la variable se describen a continuación.

#### 1. Zona

| Categoría | Codificación |
|:---------:|:------------:|
| RURAL     | `r`          |
| URBANA    | `u`          |

#### 2. Sexo/Género

| Categoría  | Codificación |
|:----------:|:------------:|
| MASCULINO  | `M`          |
| FEMENINO   | `F`          |
| NO REPORTA | `NR`         |

#### 3. Tipo Delito

##### Secuestro

| Categoría           | Codificación |
|:-------------------:|:------------:|
| SECUESTRO EXTORSIVO | `ext`        |
| SECUESTRO SIMPLE    | `sim`        |

##### Hurto de Vehículos

| Categoría            | Codificación |
|:--------------------:|:------------:|
| HURTO A MOTOCICLETAS | `mot`        |
| HURTO AUTOMOTORES    | `car`        |
#### 4. Conducta/Descripción de Conducta

##### Trata de Personas

| Categoría                                                                              | Codificación |
|:--------------------------------------------------------------------------------------:|:------------:|
| ARTÍCULO 188. TRAFICO DE MIGRANTES                                                     | `188`        |
| ARTÍCULO 188 A.- TRATA DE PERSONAS                                                     | `188A`       |
| ARTICULO 141B. TRATA DE PERSONAS EN PERSONA PROTEGIDA CON FINES DE EXPLOTACION SEXUAL" | `141B`       |
| ARTÍCULO 188 B.- TRATA DE PERSONAS CIRCUNSTANCIAS DE AGRAVACIÓN PUNITIVA.              | `188B`       |

##### Acciones subversivas
| Categoría             | Codificación |
|:---------------------:|:------------:|
| HOSTIGAMIENTO         | `hos`        |
| RETENES ILEGALES      | `ret`        |
| EMBOSCADA             | `emb`        |
| ATAQUES AERONAVES     | `aer`        |
| CONTACTO ARMADO       | `arm`        |
| ATAQUES INSTALACIONES | `ain`        |
| ASALTO POBLACIÓN      | `apo`        |
| INCURSION POBLACION   | `ipo`        |
##### Delitos contra el medio ambiente

| Categoría             | Codificación |
|:---------------------:|:------------:|
| HOSTIGAMIENTO         | `hos`        |
| RETENES ILEGALES      | `ret`        |
| EMBOSCADA             | `emb`        |
| ATAQUES AERONAVES     | `aer`        |
| CONTACTO ARMADO       | `arm`        |
| ATAQUES INSTALACIONES | `ain`        |
| ASALTO POBLACIÓN      | `apo`        |


#### 5. Clase de Bien
##### Voladura de puentes y vías

| Categoría     | Codificación |
|:-------------:|:------------:|
| PUENTE        | `p`          |
| VIAS PUBLICAS | `v`          |
##### Incautaciones de maquinaria por explotación ilícita de minerales

La clasificación para la maquinaria se divide en:
1. **Maquinaria amarilla**, codificada como `y`
2. **Otra maquinaria**, codificada como `o`. 

Las variables que agregan la maquinaria incautada en dos categorías son de la forma `n_o_maqui_7_2022`. 

| Categoría               | Codificación | Clasificación |
|:-----------------------:|:------------:|:-------------:|
| RETROESCAVADORA         | `ret`        | `y`           |
| TRITURADORA             | `trit`       | `o`           |
| CLASIFICADORA           | `clas`       | `o`           |
| DRAGA                   | `dra`        | `o`           |
| BULDOZER                | `bul`        | `y`           |
| MEZCLADORA              | `mez`        | `o`           |
| DRAGA (MINERIA)         | `dram`       | `o`           |
| EXCAVADORA              | `exc`        | `y`           |
| DRAGA TIPO DRAGON       | ``drd`       | ``o`          |
| CLASIFICADORA (MINERIA) | `clam`       | `o`           |
| DRAGA TIPO BUZO         | `drb`        | `o`           |
| MEZCLADORA (VEHICULO)   | `mezv`       | `o`           |

#### 6. Tipo de Cultivo
##### Erradicación de cultivos ilícitos

| Categoría | Codificación |
|:---------:|:------------:|
| AMAPOLA   | `amap`       |
| COCA      | `coca`       |
| MARIHUANA | `mari`       |
#### 7. Accion
##### Neutralizaciones

| Categoría                           | Codificación |
|:-----------------------------------:|:------------:|
| CAPTURADO                           | `cap`        |
| MUERTO EN DESARROLLO DE OPERACIONES | `mrt`        |

##### Afectación de la Fuerza Pública

| Categoría | Codificación |
|:---------:|:------------:|
| ASESINADO | `ase`        |
| HERIIDO   | `her`        |
#### 8. Bien
##### Incautación de equipo

| Categoría                       | Codificación |
|:-------------------------------:|:------------:|
| COMBUSTIBLE                     | `comb`       |
| MUNICION                        | `mun`        |
| ARMAMENTO                       | `arm`        |
| VEHÍCULOS TERRESTRES INCAUTADOS | `vti`        |
| EQUIPO DE COMUNICACION          | `equ`        |
| EMBARCACIONES INCAUTADAS        | `barc`       |
| GRANADAS                        | `gran`       |
| EXPLOSIVOS                      | `xplo`       |
| ARTEFACTO EXPLOSIVO             | `axpl`       |
| AERONAVES INCAUTADAS            | `aero`       |


La base consolida la información de los 42 archivos de delitos que aparecen en la siguiente tabla. En la tabla están también 2 hechos, _desmovilizados_ y _gestión del riesgo de desastres_, cuyos archivos no se pudieron descargar de la [página del Ministerio de Defensa](https://www.mindefensa.gov.co/irj/portal/Mindefensa/contenido?NavigationTarget=navurl://c17680c4c6ae25daaa589817c6ce4205).

# Tabla 1

| ID   | Núm. Sección | Título Sección                                                           | Delitos                                                                                                                                                 | Archivo                                          | Num. variables desagreg. | Desagregaciones                                                          | Nombre variable | Período disponible      |     |
|:----:|:------------:|--------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------|--------------------------|--------------------------------------------------------------------------|-----------------|-------------------------|-----|
| 1.1  | 1            | Delitos contra la vida y la integridad personal                          | Homicidio Intencional                                                                                                                                   | HOMICIDIO.xlsx                                   | 2                        | zona, sexo                                                               | homi           | 2003-01-01 a 2023-08-31 | ✅   |
| 1.2  | 1            |                                                                          | Masacres                                                                                                                                                | HOMICIDIO_COLECTIVO.xlsx                         | 1                        | zona                                                                     | mscr            | 2003-01-03 a 2023-03-23 | ✅   |
| 1.3  | 1            |                                                                          | Homicidio Colectivo (víctimas)                                                                                                                          | HOMICIDIO_COLECTIVO_VICTIMAS.xlsx                | 1                        | zona                                                                     | mscr            | 2003-01-03 a 2023-03-23 | ✅   |
| 1.4  | 1            |                                                                          | Homicidio en accidentes de transito                                                                                                                     | HOMICIDIO_ACCIDENTE_TRANSITO.xlsx                |                          |                                                                          | homacc          | 2001-01-01 a 2023-08-31 | ✅   |
| 1.5  | 1            |                                                                          | Lesiones personales                                                                                                                                     | LESIONES_PERSONALES.xlsx                         | 1                        | sexo                                                                     | lespers         | 2003-01-01 a 2023-08-31 | ✅   |
| 1.6  | 1            |                                                                          | Lesiones en accidentes de transito                                                                                                                      | LESIONES_ACCIDENTES_TRANSITO.xlsx                |                          |                                                                          | lesacc          | 2003-01-01 a 2023-08-31 | ✅   |
| 2.1  | 2            | Delitos contra la libertad individual y otras garantías constitucionales | Secuestro                                                                                                                                               | SECUESTRO.xlsx                                   | 1                        | tipo_delito                                                              | sec             | 1996-01-01 a 2023-08-30 | ✅   |
| 2.2  | 2            |                                                                          | Trata de personas                                                                                                                                       | TRATA_PERSONAS.xlsx                              | 2                        | conducta, genero                                                         | trtper          | 2003-12-27 a 2023-08-31 | ✅   |
| 3.1  | 3            | Delitos contra la libertad, integridad y formación sexuales              | Delitos sexuales                                                                                                                                        | DELITOS_SEXUALES.xlsx                            | 2                        | zona, sexo                                                               | dsex            | 2003-01-01 a 2023-08-31 | ✅   |
| 4.1  | 4            | Delitos contra el patrimonio económico                                   | Hurto de vehículos (Hurto de automotores y Hurto de motocicletas)                                                                                       | HURTO_VEHICULOS.xlsx                             | 2                        | tipo_delito, zona                                                        | hveh            | 2003-01-01 a 2023-08-31 | ✅   |
| 4.2  | 4            |                                                                          | Hurto a residencias                                                                                                                                     | HURTO_RESIDENCIAS.xlsx                           | 1                        | tipo_delito: Error en variable: tiene info sobre zona no tipo de deilto. | hresid          | 2003-01-01 a 2023-08-31 | ✅   |
| 4.3  | 4            |                                                                          | Hurto a comercio                                                                                                                                        | HURTO_COMERCIO.xlsx                              |                          |                                                                          | hcomer          | 2003-01-01 a 2023-08-31 | ✅   |
| 4.4  | 4            |                                                                          | Hurto a personas 2017 - 2021                                                                                                                            | HURTO_PERSONAS2017-2023.xlsx                     |                          |                                                                          | hperso           | 2017-01-01 a 2023-08-31 | ✅   |
| 4.5  | 4            |                                                                          | Hurto a personas 2003 - 2016                                                                                                                            | HURTO_PESONAS2003-2016.xlsx                      |                          |                                                                          |                 | 2003-01-01 a 2016-12-31 | ✅   |
| 4.6  | 4            |                                                                          | Hurto a entidades financieras                                                                                                                           | HURTO_ENTIDADES_FINANCIERAS.xlsx                 |                          |                                                                          | hfinan          | 2003-01-02 a 2023-08-29 | ✅   |
| 4.7  | 4            |                                                                          | Piratería terrestre                                                                                                                                     | PIRATERIA_TERRESTRE.xlsx                         | 1                        | zona                                                                     | pirat           | 2003-01-03 a 2023-08-23 | ✅   |
| 4.8  | 4            |                                                                          | Extorsión (Ene-1996 a Feb-2021)                                                                                                                         | EXTORSION.xlsx                                   |                          |                                                                          | extor           | 1996-01-01 a 2023-08-29 | ✅   |
| 4.9  | 4            |                                                                          | Abigeato                                                                                                                                                | ABIGEATO.xlsx                                    | 1                        | zona                                                                     | abgto           | 2003-01-01 a 2023-08-31 | ✅   |
| 5.1  | 5            | Delitos contra la familia                                                | Delitos relaciondos con la violencia intrafamiliar (desde enero de 2003).                                                                               | VIOLENCIA_INTRAFAMILIAR.xlsx                     | 1                        | zona                                                                     | vifam           | 2003-01-01 a 2018-12-31 | ✅   |
| 6.1  | 6            | Delitos contra la protección de la información y de los datos            | Delitos contra la protección de la información y de los datos (Desde 2006).                                                                             | DELITOS_INFORMATICOS.xlsx                        |                          |                                                                          | delinf          | 2006-05-13 2023-08-31   | ✅   |
| 7.1  | 7            | Delitos contra los recursos naturales y el medio ambiente                | Delitos contra los recursos naturales y el medio ambiente (desde enero de 2003).                                                                        | DELITOS_CONTRA_MEDIO_AMBIENTE.xlsx               | 2                        | descripcion_conducta, zona                                               | natur           | 2003-01-01 a 2023-08-31 | ✅   |
| 8.1  | 8            | Delitos contra la seguridad pública                                      | Actos de terrorismo (Desde ene-2003)                                                                                                                    | TERRORISMO.xlsx                                  | 1                        | zona                                                                     | terro           | 2003-01-01 a 2023-08-31 | ✅   |
| 8.2  | 8            |                                                                          | Voladura Oleoductos (Desde ene-2007)                                                                                                                    | VOLADURA_OLEODUCTOS.xlsx                         |                          |                                                                          | voleo           | 2007-01-17 a 2023-08-29 | ✅   |
| 8.3  | 8            |                                                                          | Voladura de torres (Desde ene-2007)                                                                                                                     | TORRES_ENERGIA.xlsx                              |                          |                                                                          | vtorr           | 2007-01-01 a 2018-07-16 | ⚠️  |
| 8.4  | 8            |                                                                          | Voladura de puentes y vías                                                                                                                              | PUENTES_VIAS.xlsx                                | 1                        | clase_de_bien                                                            | vpue            | 2003-01-01 a 2023-08-01 | ✅   |
| 8.5  | 8            |                                                                          | Acciones subversivas (Desde ene-2003)                                                                                                                   | ACCIONES_SUBVERSIVAS.xlsx                        | 2                        | conducta, zona                                                           | subv            | 2003-01-01 a 2022-07-05 | ✅   |
| 9.1  | 9            | Avances en la lucha contra el problema mundial de las drogas             | Erradicación de cultivos ilícitos (2007 - 2019)                                                                                                         | ERRADICACION2007-2019.xlsx                       | 2                        | tipo_de_cultivo, unidad                                                  | erra            | 2007-01-01 a 2019-12-31 | ✅   |
| 9.2  | 9            |                                                                          | Erradicación de cultivos ilícitos (2020)                                                                                                                | ERRADICACION2020.xlsx                            | 2                        | tipo_de_cultivo, unidad                                                  |                 | 2020-01-01 a 2023-08-31 | ✅   |
| 9.3  | 9            |                                                                          | Aspersión de cultivos ilícitos                                                                                                                          | ASPERSION.xlsx                                   | 1                        | unidad (ha)                                                              | asper           | 2003-01-01 a 2015-09-30 | ✅   |
| 9.4  | 9            |                                                                          | Cocaína incautada                                                                                                                                       | COCAINA.xlsx                                     | 1                        | unidad (kg)                                                              | coca            | 2010-01-01 a 2023-08-31 | ✅   |
| 9.5  | 9            |                                                                          | Heroína incautada                                                                                                                                       | HEROINA.xlsx                                     | 1                        | unidad (kg)                                                              | hero            | 2010-01-01 a 2023-08-31 | ✅   |
| 9.6  | 9            |                                                                          | Marihuana incautada                                                                                                                                     | MARIHUANA.xlsx                                   | 1                        | unidad (kg)                                                              | marih           | 2010-01-01 a 2023-08-31 | ✅   |
| 9.7  | 9            |                                                                          | Infraestructura para la producción de drogas ilícitas destruida                                                                                         | INFRAESTRUCTURAS.xlsx                            | 1                        | unidad (unidades)                                                        | infra           | 2010-01-02 a 2023-08-31 | ✅   |
| 9.8  | 9            |                                                                          | Incautaciones de insumos sólidos                                                                                                                        | INSUMOS_SOLIDOS.xlsx                             | 1                        | unidad (kg)                                                              | solid           | 2010-01-01 a 2023-08-31 | ✅   |
| 9.9  | 9            |                                                                          | Incautaciones de insumos líquidos                                                                                                                       | INSUMOS_LIQUIDOS.xlsx                            | 1                        | unidad (galón)                                                           | liqui           | 2010-01-02 a 2023-08-31 | ✅   |
| 9.10 | 9            |                                                                          | Basuco                                                                                                                                                  | BASUCO.xlsx                                      | 1                        | unidad (kg)                                                              | basuc           | 2010-01-01 a 2023-08-31 | ✅   |
| 10.1 | 10           | Resultados operacionales de la Fuerza Pública                            | Desmovilizados                                                                                                                                          | DESMOVILIZADOS.xlsx (no disponible)              | N/A                      | N/A                                                                      | N/A             | N/A                     | N/A |
| 10.2 | 10           |                                                                          | Neutralizados (capturas y muertes en desarrollo de operaciones de la Fuerza Pública)                                                                    | NEUTRALIZACIONES.xlsx                            | 1                        | accion                                                                   | neutra          | 2010-01-01 a 2021-09-30 | ✅   |
| 10.3 | 10           |                                                                          | Incautación equipo (explosivos, munición, equipos de comunicación, armamento, Artefactos explosivos improvisados, embarcaciones, aeronaves y vehículos) | 🟦 EQUIPOS.xlsx                                  | 2                        | bien, unidad_de_medida                                                   | equip           | 2010-01-01 a 2023-08-31 | ✅   |
| 11.1 | 11           | Explotación ilícita de minerales (Ene-2010 a Feb-2021)                   | Minas intervenidas                                                                                                                                      | MINAS_INTERVENIDAS.xlsx                          | 2                        | clase_de_bien, unidad_de_medida (unidades)                               | minas           | 2010-01-04 a 2023-08-31 | ✅   |
| 11.2 | 11           |                                                                          | Capturas por explotación ilícita de minerales                                                                                                           | CAPTURAS_MINERIA_ILEGAL.xlsx                     | 1                        | unidad_de_medida (persona)                                               | capmi           | 2010-08-09 a 2023-08-31 | ✅   |
| 11.3 | 11           |                                                                          | Incautaciones de maquinaria por explotación ilícita de minerales                                                                                        | INCAUTACIONES_EXPLOTACION_ILICITA_MINERALES.xlsx | 2                        | clase_de_bien, unidad_de_medida (unidades)                               | maqui           | 2010-08-23 a 2023-08-31 | ✅   |
| 12.1 | 12           | Otras variables relacionadas con la seguridad                            | Miembros de la Fuerza Pública asesinados y heridos (desde enero de 2010)                                                                                | AFECTACION_FUERZA_PUBLICA.xlsx                   | 1                        | accion                                                                   | fpub            | 2010-01-01 a 2023-08-31 | ✅   |
|      | 13           | Gestión del riesgo de desastres                                          | Actividades gestión de riesgo de desastre (desde enero de 2017).                                                                                        | GESTION_RIESGO.xlsx (no disponible)              | N/A                      | N/A                                                                      | N/A             | N/A                     | N/A |






