#-----------------------------------------------------------------------------#
# AUTHOR: 
#   Gustavo Castillo
# DESCRIPTION:
#   In this script the files from the Ministerio de Defensa that are part of 
#   the group 1, "Delitos contra la vida y la integridad personal" are
#   processed in a three step process:
#   1. Create "names_from" variable for an eventual pivot. 
#   2. Cleaning names of values of categorical variables
#   3. Information aggregation and pivoted to wide format 
#-----------------------------------------------------------------------------#

cleaning_g1 <- function(mindef_dir_path){
  source("00_packages.R")
  source("02_prepare_data/scripts/03_mindefensa/00_functions.R")
  ##########################################################################-
  # 1. Import --------------------------------
  ##########################################################################-
  
  # Check that directory exists
  cat(mindef_dir_path, file.exists(mindef_dir_path))
  
  muni <- read_csv("01_import/import/mgn_municipios_colombia.csv") %>%
    clean_names() %>% rename(cod_muni=mpio_cdpmp) %>% select(cod_muni)
  
  homicidios <- read_excel(
    path = paste0(mindef_dir_path,"HOMICIDIO.xlsx")) %>% 
    clean_names()
  
  homicidios_colectivos <- read_excel(
    path = paste0(mindef_dir_path,"HOMICIDIO_COLECTIVO.xlsx"), 
    col_types = c("date","text","text","text","text","text","numeric")) %>%
    clean_names()
  
  homicidios_colectivos_victimas <- read_excel(
    path = paste0(mindef_dir_path,"HOMICIDIO_COLECTIVO_VICTIMAS.xlsx"),
    col_types = c("date","text","text","text","text","text","numeric")) %>% 
    clean_names()
  
  homicidio_transito <- read_excel(
    path = paste0(mindef_dir_path,"HOMICIDIO_ACCIDENTE_TRANSITO.xlsx"))  %>%
    clean_names() 
  
  lesiones_transito <- read_excel(
    path = paste0(mindef_dir_path,"LESIONES_ACCIDENTES_TRANSITO.xlsx"))  %>%
    clean_names() 
  
  lesiones_personales <- read_excel(
    path = paste0(mindef_dir_path,"LESIONES_PERSONALES.xlsx")) %>%
    clean_names() 
  
  # Join massacre information with number of victims ------------------------
  # Create ID variable for each dataframe for join
  homicidios_colectivos=rowid_to_column(homicidios_colectivos, "id")
  homicidios_colectivos_victimas = rowid_to_column(
    homicidios_colectivos_victimas, "id")
  # Change "cantidad" column name to the number of victims "nvictimas"
  colnames(homicidios_colectivos_victimas)[8] = "nvictimas"
  
  # Perform inner join (merge) and drop "id" column
  masacres <- inner_join(x = homicidios_colectivos, y =  homicidios_colectivos_victimas[,c(1,8)],
                         by='id') %>% dplyr::select(!"id")
  rm(homicidios_colectivos, homicidios_colectivos_victimas)
  
  ##########################################################################-
  # 2. Clean names of categories ------------------------------------
  ##########################################################################-
  homicidios <- clean_zona(homicidios) %>% clean_sexo()
  
  masacres <- clean_zona(masacres)
  
  lesiones_personales <- clean_sexo(lesiones_personales)
  
  ##########################################################################-
  # 3. Process and join dataframes ------------------------------------
  ##########################################################################-
  ## 1. Homicidios ------------
  base_df <- step1(df = homicidios, crime_name = "homi")
  
  working_df <- Bstep2(df = base_df,aggvar = cantidad,
                       cod_muni, crime_month_year,
                       names_from = "crime_month_year",
                       prefix = "n_")
  # Merge new working_df to MGN dataframe with all 1121 municipalities
  working_df <- left_join(x=muni,
                          y=working_df,
                          by='cod_muni')
  # Add columns by sex
  working_df <- left_join(x = working_df,
                          y = Bstep2(base_df,aggvar = cantidad,
                                     cod_muni, sexo_r, crime_month_year,
                                     names_from = c(sexo_r, crime_month_year),
                                     prefix = "n_"),
                          by = "cod_muni")
  
  ## 2. Masacres ------------
  base_df <- step1(df = masacres, crime_name = "mscr")
  # Add columns of total quantity of massacres 
  y_join <- Bstep2(df = base_df, aggvar = cantidad,
                   cod_muni, crime_month_year,
                   names_from = "crime_month_year",
                   prefix = "n_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  # Add columns of quantity of massacres by zone
  y_join <- Bstep2(df = base_df, aggvar = cantidad,
                   cod_muni, zona_r, crime_month_year,
                   names_from = c(zona_r,crime_month_year),
                   prefix = "n_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  
  # Add columns of total quantity of victims of the massacres in that month-year
  y_join <- Bstep2(df = base_df, aggvar = nvictimas,
                   cod_muni, crime_month_year,
                   names_from = "crime_month_year",
                   prefix = "vict_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  # Add columns of quantity of victims by zone
  y_join <- Bstep2(df = base_df, aggvar = nvictimas,
                   cod_muni, zona_r, crime_month_year,
                   names_from = c(zona_r,crime_month_year),
                   prefix = "vict_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  
  ## 3. Homicidio en accidentes de transito ------------
  
  base_df <- step1(df = homicidio_transito, crime_name = "homacc")
  # Add columns of total quantity of accidents
  y_join <- Bstep2(df = base_df, aggvar = cantidad,
                   cod_muni, crime_month_year,
                   names_from = "crime_month_year",
                   prefix = "n_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  
  ## 4. Lesiones personales ------------
  base_df <- step1(df = lesiones_personales, crime_name = "lespers")
  # Add columns of total quantity of personal injuries
  y_join <- Bstep2(df = base_df, aggvar = cantidad,
                   cod_muni, crime_month_year,
                   names_from = "crime_month_year",
                   prefix = "n_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  # Add columns of quantity of personal injuries by sex
  y_join <- Bstep2(df = base_df, aggvar = cantidad,
                   cod_muni, sexo_r, crime_month_year,
                   names_from = c(sexo_r,crime_month_year),
                   prefix = "n_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  
  ## 5. Lesiones en accidentes de tránsito ------------
  base_df <- step1(df = lesiones_transito, crime_name = "lesacc")
  # Add columns of total quantity of injuries in car accidents
  y_join <- Bstep2(df = base_df, aggvar = cantidad,
                   cod_muni, crime_month_year,
                   names_from = "crime_month_year",
                   prefix = "n_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  
  
  # Now for the script to be easily chained to the rest of the scripts this 
  # working_df should be exported in the output for other scripts to pick up on it.
  export(working_df, file = "02_prepare_data/output/03_mindefensa/working_df.rds")
}
# End