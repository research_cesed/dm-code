#-----------------------------------------------------------------------------#
# AUTHOR: 
#   Gustavo Castillo
# DESCRIPTION:
#   In this script the files from the Ministerio de Defensa that are part of 
#   the group 3, "Delitos contra la libertad, integridad y formación sexuales"
#   are processed in a three step process:
#   1. Create "names_from" variable for an eventual pivot. 
#   2. Cleaning names of values of categorical variables
#   3. Information aggregation and pivoted to wide format 
#-----------------------------------------------------------------------------#
# Prepare workstation
rm(list=ls())
cleaning_g3 <- function(mindef_dir_path){
  source("00_packages.R")
  source("02_prepare_data/scripts/03_mindefensa/00_functions.R")
  working_df <- import("02_prepare_data/output/03_mindefensa/working_df_g2.rds")
  ##########################################################################-
  # 1. Import --------------------------------
  ##########################################################################-
  # Check that directory exists
  cat(mindef_dir_path, file.exists(mindef_dir_path))
  
  delitos_sexuales  <- read_excel(
    path = paste0(mindef_dir_path,"DELITOS_SEXUALES.xlsx"),
    # col_types = c("date","text","text","text","text","text","text","numeric")
    ) %>%
    clean_names()
  
  ##########################################################################-
  # 2. Clean names of categories ------------------------------------
  ##########################################################################-
  delitos_sexuales <- clean_zona(delitos_sexuales) %>% clean_sexo()
  
  ##########################################################################-
  # 3. Process and join dataframes ------------------------------------
  ##########################################################################-
  ## 1. Delitos Sexuales ------------
  base_df <- step1(df = delitos_sexuales, crime_name = "dsex")
  
  # Add columns of total quantity of sexual crimes 
  y_join <- Bstep2(df = base_df, aggvar = cantidad,
                   cod_muni, crime_month_year,
                   names_from = "crime_month_year",
                   prefix = "n_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  # Add columns of total quantity of sexual crimes by zone
  y_join <- Bstep2(df = base_df, aggvar = cantidad,
                   cod_muni, zona_r, crime_month_year,
                   names_from = c(zona_r,crime_month_year),
                   prefix = "n_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  # Add columns of total quantity of sexual crimes by sex
  y_join <- Bstep2(df = base_df, aggvar = cantidad,
                   cod_muni, sexo_r, crime_month_year,
                   names_from = c(sexo_r,crime_month_year),
                   prefix = "n_")
  working_df <- left_join(x = working_df,
                          y = y_join,
                          by= "cod_muni") %>% replace(is.na(.), 0)
  
  
  
  # Now for the script to be easily chained to the rest of the scripts this 
  # working_df should be exported in the output for other scripts to pick up on it.
  export(working_df, file = "02_prepare_data/output/03_mindefensa/working_df_g3.rds")
}
# End