#!/bin/bash

# Check if the number of arguments is correct
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

# Set the directory from the command-line argument
directory="$1"

# Check if the specified directory exists
if [ ! -d "$directory" ]; then
    echo "Error: Directory '$directory' not found."
    exit 1
fi

# Go to the specified directory
cd "$directory" || exit

# Function to get numeric representation of a month
get_month_number() {
    case $1 in
        "enero")      echo "01" ;;
        "febrero")    echo "02" ;;
        "marzo")      echo "03" ;;
        "abril")      echo "04" ;;
        "mayo")       echo "05" ;;
        "junio")      echo "06" ;;
        "julio")      echo "07" ;;
        "agosto")     echo "08" ;;
        "septiembre") echo "09" ;;
        "octubre")    echo "10" ;;
        "noviembre")  echo "11" ;;
        "diciembre")  echo "12" ;;
        *)            echo "unknown" ;;
    esac
}


# Loop through each .xlsx file in the directory
for file in *.xlsx; do
    # Check if the file is a regular file
    if [ -f "$file" ]; then

       # Extract month and year from the file name
       month_text=$(echo "$file" | awk '{print tolower($0)}'| grep -o -E '(enero|febrero|marzo|abril|mayo|junio|julio|agosto|septiembre|octubre|noviembre|diciembre)')
       #echo "$month_text"
       month_number=$(get_month_number "$month_text")
       year=$(echo "$file" | grep -o -E '[[:digit:]]{4}')

        # If month is not found, set it to "unknown"
        [ -z "$month" ] && month="unknown"


        # Rename the file in the desired format
        new_name="${month_number}-${year}.xlsx"
        mv "$file" "$new_name"
        echo "Renamed: $file to $new_name"
    fi
done

