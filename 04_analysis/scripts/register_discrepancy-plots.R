#-----------------------------------------------------------------------------#
# DATE:
#   14/feb/2024
# AUTHOR: 
#   Gustavo Castillo
# DESCRIPTION:
#   In this script I plot the time series for the main aggregation of all crimes
#   in the MinDefensa website, comparing the origin of the time series. Some
#   files were downloaded on october 2023 and others on january2024. 
#-----------------------------------------------------------------------------#

##############################################################################-
# 1. Prepare data -------------------------------------------------------------
##############################################################################-
rm(list=ls())
gc()
source('00_packages.R')

## 1.1. Import data ------------------------------
oct2023 <- arrow::read_parquet(
  "02_prepare_data/output/YL_allmuni_oct2023.parquet") %>% 
  select(-c('departamento','municipio'))
jan2024 <- arrow::read_parquet(
  "02_prepare_data/output/YL_allmuni_jan2024.parquet") %>% 
  select(-c('departamento','municipio'))

muni <- read_csv("01_import/import/mgn_municipios_colombia.csv") %>%
  clean_names() %>% rename(cod_muni=mpio_cdpmp) %>% 
  select(-x1)
# Crime Directory 
crimedir <- read_excel(
  path = "01_import/import/MinDefensa/mindef_crime_directory.xlsx",
  sheet = "admin"
) %>% filter(incluir=="yes")


reg <- read_csv(
  "01_import/import/Departamentos_y_municipios_de_Colombia_20240214.csv",
  col_types = cols(.default = "c")) %>% 
  clean_names() %>% 
  rename(cod_muni=codigo_dane_del_municipio)

reg <- reg %>% mutate(
  cod_muni=gsub("\\.","",cod_muni)) %>% 
  mutate(
  cod_muni=case_when(
    as.double(codigo_dane_del_departamento)<10 ~ paste0("0",cod_muni),
    .default = cod_muni)
  ) %>% 
  mutate(cod_muni=case_when(
    nchar(cod_muni)<4~paste0(cod_muni,"00"),
    nchar(cod_muni)<5~paste0(cod_muni,"0"),
    .default = cod_muni))

# Create consolidated dataframe with the region with region of municipality
muni <- left_join(muni, reg, 
          by = 'cod_muni')
muni$region %>% table(useNA = 'always')

## 1.2. Create YN dataframe for oct2023 ---------------------
oct2023_j <- left_join(oct2023, muni, by = 'cod_muni') %>% 
  mutate(file="oct2023")
jan2024_j <- left_join(jan2024, muni, by = 'cod_muni') %>% 
  mutate(file="jan2024")

oct2023j_n <- oct2023_j %>% 
  group_by(year) %>% 
  summarise(across(where(is.numeric), sum)) %>% 
  mutate(file="oct2023")

jan2024j_n <- jan2024_j %>% 
  group_by(year) %>% 
  summarise(across(where(is.numeric), sum)) %>% 
  mutate(file="jan2024")

full_df <- rbind(oct2023_j, jan2024_j)

full_df_n <- rbind(oct2023j_n, jan2024j_n)





##############################################################################-
# 2. Plot Data -------------------------------------------------------------
##############################################################################-

plot_crime <- function(crime_stub_str){
  cv <- pull(filter(crimedir, crime_stub==crime_stub_str)['values_to'])
  crime_title <- pull(filter(crimedir, crime_stub==crime_stub_str)['Archivo'])
  
  # Subset df to chosen crime
  plot_df <- full_df_n %>% 
    select(year, !!as.symbol(cv), file) %>% 
    filter(!is.na(!!as.symbol(cv)))
  
  # Create plot
  p <- ggplot(plot_df, 
              aes(x=year, y=!!as.symbol(cv), color=file, linetype=file))+
    geom_line()+
    labs(x="Año",
         y="Cantidad reportada",
         color="Archivo utilizado:",
         title = crime_title,
         caption = "Fuente: Ministerio de Defensa")+
    scale_color_manual(labels = c("oct2023" = "Reporte 2023", "jan2024" = "Reporte 2024"),
                       values = c("blue", "orange"))+
    scale_linetype_manual(labels = c("oct2023" = "Reporte 2023", "jan2024" = "Reporte 2024"),
                          values = c("solid", "dashed"))+
    guides(scale='none',  linetype = FALSE)+  # Omit the legend for linetype
    theme_bw()+
  scale_x_continuous(breaks=seq(min(plot_df$year),max(plot_df$year),1))+
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),
        legend.position = "top")
  p
    
  }
plot_crime(crime_stub_str = "vict_mscr")

plot_list <- list()
for (crime in crimedir$crime_stub){
  
  n <- plot_crime(crime_stub_str = crime)
  
  # Export graph
  f <- paste0("04_analysis/output/figures/",crime,"_2023v2024.jpg")
  ggsave(f,
         width=15,
         height=10,
         units = "cm")
  plot_list[[crime]] <- n
}


# Filter only those crimes whose yearly report is not consistent between files:
for (y in seq(1996,2023, 1)){
  print(y)
  
  cat("Old sum: ")
  
  cat("\n\n")
}
